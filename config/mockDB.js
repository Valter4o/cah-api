var mongodb = require("mongo-mock");
var MongoClient = mongodb.MongoClient;
var url = "mongodb://localhost:27017/test";

// Always writes on the file
// MongoClient.persist = "mongo.js";

function createMockDB(collections) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url, {}, async function (err, client) {
      if (err) reject(err);
      var db = client.db();

      for (const [collectionName, docs] of Object.entries(collections)) {
        var collection = db.collection(collectionName);
        await collection.insertMany(docs);
      }
      resolve(db);
    });
  });
}

module.exports = createMockDB;
