const { auth, home, game } = require("../routes");
const cards = require("../Cards");
const cool = require("cool-ascii-faces");

module.exports = (app) => {
  app.use("/auth", auth);
  app.use("/homeText", home);
  app.use("/deck", cards);
  app.use("/game", game);
  app.use("/cool", (req, res) => res.send(cool()));
};
