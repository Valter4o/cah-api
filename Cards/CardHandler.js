const DatabaseService = require("./DB");
const CardServices = require("./CardServices");

const cardService = new CardServices(DatabaseService);
module.exports = {
  getAllDecks(req, res) {
    cardService.getAllDecks().then((data) => {
      res.json(data);
    });
  },
  getOneDeck(req, res) {
    const context = {
      deckId: req.params.deckId,
    };
    cardService.getDeckById(context).then((data) => {
      res.json(data);
    });
  },
  getAllCardsByType(req, res, type) {
    const context = {
      type,
    };
    cardService.getAllCardsByType(context).then((data) => {
      res.json(data);
    });
  },
  getAllCardsFromDeck(req, res, type) {
    const context = { deckId: req.params.deckId, type };
    cardService.getAllCardsFromDeckByType(context).then((data) => {
      res.json(data);
    });
  },
  getCountCardsFromDeck(req, res, type) {
    const context = {
      deckId: req.params.deckId,
      type,
      count: req.params.count,
    };

    cardService.getCountCardsFromDeckByType(context).then((data) => {
      res.json(data);
    });
  },
};
