const { ObjectId } = require("mongodb");
const { Schema, model: Model } = require("mongoose");
const { String } = Schema.Types;
const random = require("mongoose-random");

const deckSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  cards: {
    black: [
      {
        type: ObjectId,
        required: true,
      },
    ],
    white: [
      {
        type: ObjectId,
        required: true,
      },
    ],
  },
});
deckSchema.plugin(random, { path: "r" });

module.exports = new Model("Deck", deckSchema);
