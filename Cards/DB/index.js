const Deck = require("./DeckModel");
const BlackCard = require("./BlackCardModel");
const WhiteCard = require("./WhiteCardModel");

const MODELS = {
  deck: Deck,
  white: WhiteCard,
  black: BlackCard,
};

module.exports = {
  getAll(model) {
    return MODELS[model].find();
  },
  getByFilter(model, filter) {
    return MODELS[model].find(filter);
  },
};
