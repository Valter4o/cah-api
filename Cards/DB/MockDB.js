const mockDB = require("../../config/mockDB");

const MockModels = {
  deck: [
    {
      _id: "deckId1",
      name: "deckName1",
      cards: {
        white: ["whiteCardId1", "whiteCardId2", "whiteCardId3"],
        black: ["blackCardId1", "blackCardId2", "blackCardId3"],
      },
    },
    {
      _id: "deckId2",
      name: "deckName2",
      cards: {
        white: ["whiteCardId2", "whiteCardId3", "whiteCardId4"],
        black: ["blackCardId2", "blackCardId3", "blackCardId4"],
      },
    },
  ],
  white: [
    {
      _id: "whiteCardId1",
      name: "whiteCardName1",
    },
    {
      _id: "whiteCardId2",
      name: "whiteCardName2",
    },
    {
      _id: "whiteCardId3",
      name: "whiteCardName3",
    },
    {
      _id: "whiteCardId4",
      name: "whiteCardName4",
    },
  ],
  black: [
    {
      _id: "blackCardId1",
      name: "blackCardName1",
      pickTwo: "true",
    },
    {
      _id: "blackCardId2",
      name: "blackCardName2",
      pickTwo: "false",
    },
    {
      _id: "blackCardId3",
      name: "blackCardName3",
      pickTwo: "false",
    },
    {
      _id: "blackCardId4",
      name: "blackCardName4",
      pickTwo: "false",
    },
  ],
};

let dbInstance;

function getDB(MockModels) {
  return new Promise((resolve, reject) => {
    if (dbInstance) {
      resolve(dbInstance);
    } else {
      mockDB(MockModels).then((db) => {
        dbInstance = db;
        resolve(dbInstance);
      });
    }
  });
}

module.exports = {
  async getAll(model) {
    const db = await getDB(MockModels);
    return db.collection(model).find().toArray();
  },
  async getByFilter(model, filter) {
    const db = await getDB(MockModels);
    return db.collection(model).find(filter).toArray();
  },
};
