const shuffle = require("../utils/shuffle");

class CardServices {
  constructor(DatabaseService) {
    this.DB = DatabaseService;
  }
  getAllDecks() {
    return this.DB.getAll("deck");
  }
  getDeckById({ deckId }) {
    return this.DB.getByFilter("deck", { _id: deckId });
  }
  getAllCardsByType({ type }) {
    return this.DB.getAll(type);
  }
  getAllCardsFromDeckByType({ deckId, type }) {
    return new Promise((resolve, reject) => {
      this.DB.getByFilter("deck", { _id: deckId })
        .then(([{ cards }]) => {
          const idsArray = cards[type];

          this.DB.getByFilter(type, {
            _id: {
              $in: idsArray,
            },
          })
            .then((cards) => {
              resolve(cards);
            })
            .catch((err) => {
              reject(err);
            });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
  getCountCardsFromDeckByType({ deckId, type, count }) {
    return new Promise((resolve, reject) => {
      this.DB.getByFilter("deck", { _id: deckId })
        .then(([{ cards }]) => {
          const idsArray = shuffle(cards[type]).slice(0, count);

          this.DB.getByFilter(type, {
            _id: {
              $in: idsArray,
            },
          })
            .then((cards) => {
              resolve(cards);
            })
            .catch((err) => {
              reject(err);
            });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
}

module.exports = CardServices;
