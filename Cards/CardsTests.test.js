const MockDBService = require("./DB/MockDB");
const CardServices = require("./CardServices");

const cardService = new CardServices(MockDBService);

describe("CardService Test Suite", () => {
  test(".getAllDecks() returns 2 decks", async () => {
    await cardService.getAllDecks().then((data) => {
      expect(data).toHaveLength(2);
    });
  });

  test(".getDeckById() returns 1 deck", async () => {
    await cardService.getDeckById({ deckId: "deckId2" }).then((data) => {
      expect(data).toHaveLength(1);
    });
  });

  test(".getDeckById() returns the deck whith the given 1 id", async () => {
    await cardService.getDeckById({ deckId: "deckId2" }).then((data) => {
      expect(data[0]._id).toEqual("deckId2");
    });
  });

  test(".getDeckById() doesn't return the deck that is not the given", async () => {
    await cardService.getDeckById({ deckId: "deckId2" }).then((data) => {
      expect(data[0]._id).not.toEqual("deckId1");
    });
  });

  test(".getAllCardsByType() with type = black returns 4 black cards", async () => {
    await cardService.getAllCardsByType({ type: "black" }).then((data) => {
      expect(data).toHaveLength(4);
    });
  });

  test(".getAllCardsByType() with type = black returns only black cards", async () => {
    await cardService.getAllCardsByType({ type: "black" }).then((data) => {
      data = data.filter((el) => el.pickTwo !== undefined);
      expect(data).toHaveLength(4);
    });
  });

  test(".getAllCardsByType() with type = white returns 4 white cards", async () => {
    await cardService.getAllCardsByType({ type: "white" }).then((data) => {
      expect(data).toHaveLength(4);
    });
  });

  test(".getAllCardsByType() with type = white returns only white cards", async () => {
    await cardService.getAllCardsByType({ type: "white" }).then((data) => {
      data = data.filter((el) => el.pickTwo === undefined);
      expect(data).toHaveLength(4);
    });
  });

  test(".getAllCardsFromDeckByType() with deckId and type = black returns 3 black cards", async () => {
    await cardService
      .getAllCardsFromDeckByType({ deckId: "deckId1", type: "black" })
      .then((data) => {
        expect(data).toHaveLength(3);
      });
  });

  test(".getAllCardsFromDeckByType() with deckId and type = black returns the black cards listed in the deck", async () => {
    await cardService
      .getAllCardsFromDeckByType({ deckId: "deckId1", type: "black" })
      .then((data) => {
        expect(data).toEqual([
          {
            _id: "blackCardId1",
            name: "blackCardName1",
            pickTwo: "true",
          },
          {
            _id: "blackCardId2",
            name: "blackCardName2",
            pickTwo: "false",
          },
          {
            _id: "blackCardId3",
            name: "blackCardName3",
            pickTwo: "false",
          },
        ]);
      });
  });

  test(".getAllCardsFromDeckByType() with deckId and type = white returns 3 white cards", async () => {
    await cardService
      .getAllCardsFromDeckByType({ deckId: "deckId1", type: "white" })
      .then((data) => {
        expect(data).toHaveLength(3);
      });
  });

  test(".getAllCardsFromDeckByType() with deckId and type = white returns the white cards listed in the deck", async () => {
    await cardService
      .getAllCardsFromDeckByType({ deckId: "deckId1", type: "white" })
      .then((data) => {
        expect(data).toEqual([
          {
            _id: "whiteCardId1",
            name: "whiteCardName1",
          },
          {
            _id: "whiteCardId2",
            name: "whiteCardName2",
          },
          {
            _id: "whiteCardId3",
            name: "whiteCardName3",
          },
        ]);
      });
  });

  test(".getCountCardsFromDeckByType() with deckId and type = black returns count cards", async () => {
    await cardService
      .getCountCardsFromDeckByType({
        deckId: "deckId1",
        type: "black",
        count: "2",
      })
      .then((data) => {
        expect(data).toHaveLength(2);
      });
  });

  test(".getCountCardsFromDeckByType() with deckId and type = black returns card from cards in the deck", async () => {
    await cardService
      .getCountCardsFromDeckByType({
        deckId: "deckId1",
        type: "black",
        count: "1",
      })
      .then((data) => {
        const assertion = [
          "blackCardId1",
          "blackCardId2",
          "blackCardId3",
        ].includes(data[0]._id);

        expect(assertion).toEqual(true);
      });
  });

  test(".getCountCardsFromDeckByType() with deckId and type = black doesn't return card from cards in the deck", async () => {
    await cardService
      .getCountCardsFromDeckByType({
        deckId: "deckId1",
        type: "black",
        count: "1",
      })
      .then((data) => {
        expect(data[0]._id).not.toEqual("blackCardId4");
      });
  });

  test(".getCountCardsFromDeckByType() with deckId and type = white returns count cards", async () => {
    await cardService
      .getCountCardsFromDeckByType({
        deckId: "deckId1",
        type: "white",
        count: "2",
      })
      .then((data) => {
        expect(data).toHaveLength(2);
      });
  });

  test(".getCountCardsFromDeckByType() with deckId and type = white returns card from cards in the deck", async () => {
    await cardService
      .getCountCardsFromDeckByType({
        deckId: "deckId1",
        type: "white",
        count: "1",
      })
      .then((data) => {
        const assertion = [
          "whiteCardId1",
          "whiteCardId2",
          "whiteCardId3",
        ].includes(data[0]._id);

        expect(assertion).toEqual(true);
      });
  });

  test(".getCountCardsFromDeckByType() with deckId and type = white doesn't return card from cards in the deck", async () => {
    await cardService
      .getCountCardsFromDeckByType({
        deckId: "deckId1",
        type: "white",
        count: "1",
      })
      .then((data) => {
        expect(data[0]._id).not.toEqual("whiteCardId4");
      });
  });

  
});
