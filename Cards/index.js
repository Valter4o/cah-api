const router = require("express").Router();
const handler = require("./CardHandler");

router.get("/", handler.getAllDecks);
router.get("/white", (req, res) => {
  handler.getAllCardsByType(req, res, "white");
});
router.get("/black", (req, res) => {
  handler.getAllCardsByType(req, res, "black");
});
router.get("/:deckId", handler.getOneDeck);
router.get("/white/:deckId", (req, res) => {
  handler.getAllCardsFromDeck(req, res, "white");
});
router.get("/black/:deckId", (req, res) => {
  handler.getAllCardsFromDeck(req, res, "black");
});
router.get("/white/:deckId/:count", (req, res) => {
  handler.getCountCardsFromDeck(req, res, "white");
});
router.get("/black/:deckId/:count", (req, res) => {
  handler.getCountCardsFromDeck(req, res, "black");
});
router.post("/");

module.exports = router;
